'use strict';
$(document).ready(function() {

  var $formMeses = $('#formMeses');

  $formMeses.submit(function(e) {
    e.preventDefault();

    let mes1 = $formMeses.find('#mes1').val();
    let mes2 = $formMeses.find('#mes2').val();
    let palabras = traerPalabras(mes1,mes2);
    palabras = calcularPorcentaje( palabras);
    dibujarGrafico( palabras );
    return false;
  });

  let traerPalabras = function(mes1,mes2) {
    let palabras = null;
    $.ajax({
      url: BASE_URL+'listRepeticionPalabraPorRangoMeses/'+mes1+'/'+mes2,
      type: 'POST',
      dataType: 'json',
      async: false,
      success: function( response ) {
        palabras = response;
      }
    })
    .done(function() {
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    return palabras;
  }

  let calcularPorcentaje = function( palabras ) {

    //obtener total
    let totalPalabras = 0;
    $.each(palabras, function(index, palabra) {
      totalPalabras+= parseInt( palabra.total );
    });

    //sacar porcentaje
    $.each(palabras, function(index, palabra) {
      palabra.porcentaje = (parseInt(palabra.total)*100)/totalPalabras;
    });

    return palabras;
  }

  let dibujarGrafico = function(palabras) {
    let dataPalabras = [];

    $.each(palabras, function(index, palabra) {
      let dataPalabra = { name:palabra.nombre, y: parseInt(palabra.porcentaje), total:parseInt(palabra.total), drilldown: null};
      dataPalabras.push( dataPalabra );
    });

    Highcharts.chart('grafico', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Raking palabras'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Total de veces usadas'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                    // format: '{point.y} Veces'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.total}</b> veces usada<br/>'
        },

        series: [{
            name: 'Palabras',
            colorByPoint: true,
            data : dataPalabras
            /*data: [{
                name: 'Microsoft Internet Explorer',
                y: 56.33,
                drilldown: 'Microsoft Internet Explorer'
            }, {
                name: 'Chrome',
                y: 24.03,
                drilldown: 'Chrome'
            }, {
                name: 'Firefox',
                y: 10.38,
                drilldown: 'Firefox'
            }, {
                name: 'Safari',
                y: 4.77,
                drilldown: 'Safari'
            }, {
                name: 'Opera',
                y: 0.91,
                drilldown: 'Opera'
            }, {
                name: 'Proprietary or Undetectable',
                y: 0.2,
                drilldown: null
            }]*/
        }]
    });
  }

  
  
});//ready