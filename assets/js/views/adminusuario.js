'use strict';
$(document).ready(function() {
  var $modalEliminarUsuario = $('#modalEliminarUsuario'),
    $modalUsuario = $('#modalUsuario'),
    $formUsuario = $('#formUsuario');

  $formUsuario.find('.dateDDMMYYYY').datepicker();

  $('.btnEliminarUsuario').click( function(e) {
    //llamar eliminar usuario

    let idUsuario = $(this).attr('idUsuario');
    $modalEliminarUsuario.find('#idUsuario').val( idUsuario );
    $modalEliminarUsuario.modal('show')
  });

  $('#doEliminarUsuario').click(function(e) {
    let idUsuario = $modalEliminarUsuario.find('#idUsuario').val();

    $.ajax({
      url: BASE_URL+'eliminarUsuario/'+idUsuario,
      type: 'POST',
      dataType: 'json',
      success: function( response ) {
        location.reload();
      }
    })
    .done(function() {
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    
  });

  $('.btnEditarUsuario').click( function() {
    let idUsuario = $(this).attr('idUsuario');

    $.ajax({
      url: BASE_URL+'getUsuarioByIdUsuario/'+idUsuario,
      type: 'POST',
      dataType: 'json',
      success: function( usuario ) {
        $formUsuario.loadFormFromJSON( usuario );
        $formUsuario.find('#Clave').val('');
        $modalUsuario.modal('show');
      }
    })
    .done(function() {
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
  });


  $formUsuario.submit(function(e) {
    e.preventDefault();

    let usuario = $formUsuario.serializeObject();
    $.ajax({
      url: BASE_URL+'usuario/actualizarPerfil',
      type: 'POST',
      dataType: 'json',
      data: usuario,
      success: function(response) {
        notificar('Perfil actualizado','info');
        console.log(response);
      }
    })
    .done(function() {
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    
  });
  
});//ready
