$(document).ready(function() {
  $formSugerencia = $('#formSugerencia');

  $formSugerencia.submit(function(e) {
    e.preventDefault();

    let sugerencia = $formSugerencia.serializeObject();

    $.ajax({
      url: BASE_URL+'sugerencia/enviarSugerencia',
      type: 'POST',
      dataType: 'json',
      data: sugerencia,
      success: function( respose ) {
        notificar('Sugerencia enviada','info');
        $formSugerencia.reset();
      }
    })
    .done(function() {
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    
  });
});//ready