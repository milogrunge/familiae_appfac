'use strict';
$(document).ready(function() {
  var $formFechas = $('#formFechas'),
      $modalGrafico = $('#modalGrafico'),
      $modalFrases = $('#modalFrases');

  // Select a Date Range with datepicker
  $( "#fechaDesde" ).datepicker({
    defaultDate: "+1w",
    changeMonth: true,
    numberOfMonths: 3,
    onClose: function( selectedDate ) {
      $( "#fechaHasta" ).datepicker( "option", "minDate", selectedDate );
    }
  });
  $( "#fechaHasta" ).datepicker({
    defaultDate: "+1w",
    changeMonth: true,
    numberOfMonths: 3,
    onClose: function( selectedDate ) {
      $( "#fechaDesde" ).datepicker( "option", "maxDate", selectedDate );
    }
  });


  $formFechas.submit(function(e) {
    e.preventDefault();
  });

/***********************FRASES*********************************/
  $('#btnFrases').click( function(e) {
    $formFechas.submit();

    var desde = $formFechas.find('#fechaDesde').val().replace(/\//g,'-');
    var hasta = $formFechas.find('#fechaHasta').val().replace(/\//g,'-');

    if( desde=='' || hasta=='') {
      notificar('Seleccione un rango de fechas','warning')
      return false;
    }

    let frases = traerFrases(desde,hasta);

    frases = concatenarFrases( frases );

    //mostrar frases
    $modalFrases.find('#frases li').remove();
    $.each(frases, function(index, frase) {
      let $li = $('<li>').addClass('list-group-item').text( frase );
      $modalFrases.find('#frases').append( $li );
    });

    $modalFrases.modal('show');
  });

  let concatenarFrases = function(frases) {
    let concatenadas = {};
    $.each(frases, function(index, frase) {
      if( concatenadas[frase.idFrase] ) {
        concatenadas[frase.idFrase]+=' '+frase.Nombre;
      } else {
        concatenadas[frase.idFrase]=frase.Nombre;
      }
    });
    console.log('concatenadas: '+concatenadas);
    return concatenadas;
  }
  
  let traerFrases = function(desde, hasta) {
    let frases = null
    $.ajax({
      url: BASE_URL+'listFrasesByIdUsuario/'+desde+'/'+hasta,
      type: 'POST',
      dataType: 'json',
      async:false,
      success: function( response ) {
        frases = response;
      }
    })
    .done(function() {
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    return frases;
  }







  /****************************Aprendizaje********************************/
  $('#btnAprendizaje').click( function(e) {
    $formFechas.submit();

    var desde = $formFechas.find('#fechaDesde').val().replace(/\//g,'-');
    var hasta = $formFechas.find('#fechaHasta').val().replace(/\//g,'-');

    if( desde=='' || hasta=='') {
      notificar('Seleccione un rango de fechas','warning')
      return false;
    }
    let aprendizaje = traerAprendizaje(desde,hasta);
    
    //calcular porcentaje
    aprendizaje.porcCorrecto = (aprendizaje.correcto*100) / aprendizaje.total;
    aprendizaje.porcIncorrecto = (aprendizaje.incorrecto*100) / aprendizaje.total;

    dibujarGraficoAprendizaje( aprendizaje );
    $modalGrafico.modal('show');
    return false;
  });

  let traerAprendizaje = function(desde, hasta) {
    let aprendizaje = null
    $.ajax({
      url: BASE_URL+'getHistorialAprendizaje/'+desde+'/'+hasta,
      type: 'POST',
      dataType: 'json',
      async:false,
      success: function( response ) {
        aprendizaje = response;
      }
    })
    .done(function() {
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    return aprendizaje;
  }

  let dibujarGraficoAprendizaje = function(aprendizaje) {
    
    Highcharts.chart('grafico', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Aprendizaje'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'valor'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                    // format: '{point.y} Veces'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.total}</b> veces usada<br/>'
        },

        series: [{
            name: 'Palabras',
            colorByPoint: true,
            data: [{
                name: 'Correctas',
                y: aprendizaje.porcCorrecto,
            }, {
                name: 'Erroneas',
                y: aprendizaje.porcIncorrecto,
            }]
        }]
    });
  }



















/***************PALABRAS*********************************/
  $('#btnPalabras').click( function(e) {
    $formFechas.submit();

    var desde = $formFechas.find('#fechaDesde').val().replace(/\//g,'-');
    var hasta = $formFechas.find('#fechaHasta').val().replace(/\//g,'-');

    if( desde=='' || hasta=='') {
      notificar('Seleccione un rango de fechas','warning')
      return false;
    }
    let palabras = traerPalabras(desde,hasta);
    palabras = calcularPorcentajePalabras( palabras);
    dibujarGraficoPalabras( palabras );
    $modalGrafico.modal('show');
    return false;
  });


  let traerPalabras = function(desde, hasta) {
    let palabras = null;
    $.ajax({
      url: BASE_URL+'listRepeticionPalabrasByIdUsuarioAndRango/'+desde+'/'+hasta,
      type: 'POST',
      dataType: 'json',
      async:false,
      success: function( response ) {
        palabras = response;
      }
    })
    .done(function() {
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    console.log(palabras);
    return palabras;
  }

  let calcularPorcentajePalabras = function( palabras ) {

    //obtener total
    let totalPalabras = 0;
    $.each(palabras, function(index, palabra) {
      totalPalabras+= parseInt( palabra.total );
    });

    //sacar porcentaje
    $.each(palabras, function(index, palabra) {
      palabra.porcentaje = (parseInt(palabra.total)*100)/totalPalabras;
    });

    return palabras;
  }


  let dibujarGraficoPalabras = function(palabras) {
    let dataPalabras = [];

    $.each(palabras, function(index, palabra) {
      let dataPalabra = { name:palabra.nombre, y: parseInt(palabra.porcentaje), total:parseInt(palabra.total), drilldown: null};
      dataPalabras.push( dataPalabra );
    });

    Highcharts.chart('grafico', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Raking palabras'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Total de veces usadas'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                    // format: '{point.y} Veces'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.total}</b> veces usada<br/>'
        },

        series: [{
            name: 'Palabras',
            colorByPoint: true,
            data : dataPalabras
            /*data: [{
                name: 'Microsoft Internet Explorer',
                y: 56.33,
                drilldown: 'Microsoft Internet Explorer'
            }, {
                name: 'Chrome',
                y: 24.03,
                drilldown: 'Chrome'
            }, {
                name: 'Firefox',
                y: 10.38,
                drilldown: 'Firefox'
            }, {
                name: 'Safari',
                y: 4.77,
                drilldown: 'Safari'
            }, {
                name: 'Opera',
                y: 0.91,
                drilldown: 'Opera'
            }, {
                name: 'Proprietary or Undetectable',
                y: 0.2,
                drilldown: null
            }]*/
        }]
    });
  }

});//ready