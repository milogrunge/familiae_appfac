$(document).ready(function() {
  var $formUsuario = $('#formUsuario');

  $formUsuario.find('.dateDDMMYYYY').datepicker();

  $formUsuario.submit(function(e) {
    e.preventDefault();

    let usuario = $formUsuario.serializeObject();
    $.ajax({
      url: BASE_URL+'usuario/actualizarPerfil',
      type: 'POST',
      dataType: 'json',
      data: usuario,
      success: function(response) {
        notificar('Perfil actualizado','info');
      }
    })
    .done(function() {
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    
  });
});