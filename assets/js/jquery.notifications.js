/**
*Autor: Emilio Ahumada
*Organización: INGCER
*
**/
//Construir notificación
//@param mensaje (texto que se mostrará)
//@param clase (error, info);
var notificar = function(mensaje,clase){
  var notificacion = $('<div></div>')
    .addClass('notification')
    .addClass(clase)
    .append($('<label></label>')
    	.html(mensaje));
  notificacion.hide();
  notificacion.click(function(event) {
  	$(this).stop().slideUp('400', function() {
  	  $(this).remove();
  	});
  });
  $('#notificaciones-wrapper').prepend(notificacion);
  notificacion.slideDown('400', function() {
  	notificacion.delay(5000).slideUp('400',function(){
  	  notificacion.remove();
  	});
  });
}

$(document).ready(function($) {
  //agregar el wrapper para las notificaciones
  var elDiv=$('<div></div>').attr('id','notificaciones-wrapper');
  $('body').prepend(elDiv);
});