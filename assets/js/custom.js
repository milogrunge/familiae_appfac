var transparentDemo = true;
var fixedTop = false;
const BASE_URL = 'http://app.familiaescuela.cl/'; //local
// const BASE_URL = 'http://app.familiaescuela.cl/'; //Server


$(window).scroll(function(e) {
    oVal = ($(window).scrollTop() / 170);
    $(".blur").css("opacity", oVal);
    
});

$.fn.serializeObject = function() {
  var o = {};
  var a = this.serializeArray();
  $.each(a, function() {
    if(this.name.indexOf('input-no-') != -1) {
      return;
    } else if(this.name.indexOf('.') < 0) {
      o[this.name] = this.value;
    } else {
      var attributes = this.name.split('.');
      var actual = o;
      for (var i = 0; i < attributes.length - 1; i++) {
        var att = attributes[i];
        if (actual[att] == undefined){
         actual[att] = new Object();
        }
        actual = actual[att];
      }
      actual[attributes[attributes.length - 1]] = this.value;
    }
  });
  return o;
};


$.fn.reset = function () {
  $(this).find('input[type=hidden]').val('');
  $(this).find('select :selected').removeAttr('selected');
  $(this).find('textarea').text('');
  $(this).find('select').find('option[value=""]').attr('selected','selected');  
  $(this).find('input[type=checkbox]').attr('checked',false).trigger('change');
  $(this).find('input, select, textarea').removeAttr('title').removeClass('error');
  $(this).each(function() { 
    this.reset(); 
  });
//  $(this).valid();
};


$.fn.loadFormFromJSON = function(object){
  var form =this;
  $(form).reset();
  $(this).find(':input').each(function(index, el) {
    var key=$(el).attr('id');
    if(el.type=='checkbox'){
      if(object[key]=='S' || object[key]=='s' || object[key]=='true' || object[key]=='TRUE' || object[key]==true){
        $(el).prop('checked', true);
      } else{
        $(el).prop('checked', false);
      }
      $(el).trigger('change');
    } else if(el.type=='radio') {
      $(form).find('input:radio[name="'+el.name+'"]').filter('[value="'+object[el.name]+'"]').prop('checked',true);
    } else {
      $(el).val(object[key]);
    }
  });

  $(form).trigger('jsonLoaded');
  return $(form);
}
