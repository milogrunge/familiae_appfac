<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Usuario extends CI_Controller {
  function __construct(){
    parent::__construct();
    // $this->load->helper('form');
    // $this->load->library('form_validation');
    $this->load->library('session');
    $this->load->model('date_model');
    $this->load->model('comuna_model');
    $this->load->model('usuario_model');
  }

  function index(){
    $this->load->helper(array('form'));

    log_message('error',json_encode($this->session->userdata));

    if( $this->session->userdata['idPerfil'] == ID_PERFIL_ADMINISTRADOR ) {
      header('Location:'.base_url().'admin');
    }
    $this->load->view('usuario_view');
  }

  public function cargarVistaEditarUsuario() {
    $comunas = $this->comuna_model->listAllComuna();
    $usuario = $this->usuario_model->getUsuarioByIdUsuario( $this->session->userdata['idUsuario'] );
    $usuario[0]->Nacimiento = $this->date_model->mySQLDateToFronDate( $usuario[0]->Nacimiento );

    $this->load->view('usuario/editarperfil_view',['comunas'=>$comunas, 'usuario'=>$usuario[0]]);
  }
  public function cargarVistaReportesUsuario() {
    $this->load->view('usuario/reportesusuario_view');
  }
  public function cargarVistaEnviarSugerencia() {
    $this->load->view('usuario/enviarsugerencia_view');
  }

  public function actualizarPerfil() {
    $usuario = $this->input->post();
    $usuario['Nacimiento'] = $this->date_model->frontDateToMySQLDate( $usuario['Nacimiento'] );
    echo json_encode( $this->usuario_model->actualizarPerfil( $usuario ) );
  }

}
?>