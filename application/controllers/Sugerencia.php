<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Sugerencia extends CI_Controller {
  function __construct(){
    parent::__construct();
    // $this->load->helper('form');
    // $this->load->library('form_validation');
    $this->load->library('session');
    $this->load->model('sugerencia_model');
  }

  public function enviarSugerencia() {
    $sugerencia = $this->input->post();

    $sugerencia['Usuario_idUsuario'] = $this->session->userdata['idUsuario'];

    echo json_encode(  $this->sugerencia_model->enviarSugerencia($sugerencia) );
  }

  public function verSugerencias() {

    $sugerencias = $this->sugerencia_model->listAllSugerencias();

    $this->load->view('versugerencias_view',['sugerencias'=>$sugerencias]);
  }
}