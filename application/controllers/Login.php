<?php 
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Login extends CI_Controller {

  function __construct(){
    parent::__construct();
    $this->load->helper('form');
    $this->load->library('form_validation');
    $this->load->library('session');
    $this->load->model('login_model');
  }

  function index(){
    $this->load->helper(array('form'));
    if(isset($this->session->userdata['logged_in'])){
      $this->session->userdata['idPerfil']==ID_PERFIL_ADMINISTRADOR? header('Location:'.base_url().'admin') : header('Location:'.base_url().'usuario');
    }
    $this->load->view('login_view');
  }

  public function user_login_process() {
      //Valida los campos 
    $this->form_validation->set_rules('Usuario', 'Usuario', 'trim|required|xss_clean');
    $this->form_validation->set_rules('Clave', 'Clave', 'trim|required|xss_clean');      
    if ($this->form_validation->run() == FALSE) {
      if(isset($this->session->userdata['logged_in'])){

        header('Location:'.base_url().'home_view.php');
        // $this->load->view('home_view');
      }else{
        $this->load->view('login_view');
      }
    }else{
      $data = array(    'uuname' => $this->input->post('Usuario'),
        'upass' => md5($this->input->post('Clave')));
      $result = $this->login_model->login( $this->input->post('Usuario'),$this->input->post('Clave') );           //Reemplazar por MD5
      if ($result == TRUE) {
        $Usuario = $this->input->post('Usuario');

        $result = $this->login_model->read_user_information($Usuario);

        if ($result != false) {
          $session_data = array(

            'Usuario' => $result[0]->Usuario,
            'Nombre' => $result[0]->Nombre,
            'ApellidoP' => $result[0]->ApellidoP,
            'ApellidoM' => $result[0]->ApellidoM,
            'idUsuario'=> $result[0]->idUsuario,
            'idPerfil'=> $result[0]->Perfil_idPerfil,
            'logged_in'=>true
            );  
          // Pasa el arreglo a la vista
          $this->session->set_userdata($session_data);  
          
          $result[0]->idPerfil==ID_PERFIL_ADMINISTRADOR? header('Location:'.base_url().'admin') : header('Location:'.base_url().'usuario');
          // $this->load->view('home_view');

        }
      }else{
        $data = array('error_message' => 'Usuario o Password No válidos.');
        $this->load->view('login_form', $data);
      }
    }
  }
  public function logout() {

    // Elimina los datos de la sesión
    $sess_array = array(
      'Usuario' => '',
      'Nombre' => '',
      'ApellidoP' => '',
      'ApellidoM' => '',
      'idUsuario' => '',
      'idPerfil' => '',
      'logged_in' => ''
    );
    $this->session->unset_userdata($sess_array);
    $this->session->unset_userdata('logged_in');
    $data['message_display'] = 'La sesión finalizó correctamente.';
    header('Location:'.base_url());
  }

}
?>