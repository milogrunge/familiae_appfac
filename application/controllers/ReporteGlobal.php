<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class ReporteGlobal extends CI_Controller {
  function __construct(){
    parent::__construct();
    $this->load->helper('form');
    $this->load->library('form_validation');
    $this->load->library('session');
    $this->load->model('palabra_model');
    $this->load->model('date_model');
    $this->load->model('aprendizaje_model');
    $this->load->model('frase_model');
  }

  function index(){
    $this->load->helper(array('form'));

    if( $this->session->userdata['idPerfil'] != ID_PERFIL_ADMINISTRADOR ) {
      header('Location:'.base_url().'usuario');
    }

    $this->load->view('reporte_global');
  }

  public function listRepeticionPalabraPorRangoMeses($mes1,$mes2) {
    $palabras = $this->palabra_model->listRepeticionPalabraPorRangoMeses($mes1,$mes2);

    echo json_encode($palabras);
  }

  public function listRepeticionPalabrasByIdUsuarioAndRango($desde, $hasta) {
    $desde = $this->date_model->frontDateToMySQLDate( $desde );
    $hasta = $this->date_model->frontDateToMySQLDate( $hasta );
    $idUsuario = $this->session->userdata['idUsuario'];

    $palabras = $this->palabra_model->listRepeticionPalabrasByIdUsuarioAndRango($desde,$hasta, $idUsuario);

    echo json_encode($palabras);
  }

  public function getHistorialAprendizaje($desde,$hasta) {
    $desde = $this->date_model->frontDateToMySQLDate( $desde );
    $hasta = $this->date_model->frontDateToMySQLDate( $hasta );
    $idUsuario = $this->session->userdata['idUsuario'];

    $correcto = $this->aprendizaje_model->contarAprendizajeCorrecto($desde,$hasta,$idUsuario);

    // echo json_encode($correcto);

    $incorrecto = $this->aprendizaje_model->contarAprendizajeIncorrecto($desde,$hasta,$idUsuario);
    $aprendizaje = new stdClass();
    $aprendizaje->correcto = $correcto[0]->total;
    $aprendizaje->incorrecto = $incorrecto[0]->total;
    $aprendizaje->total = $aprendizaje->correcto + $aprendizaje->incorrecto;

    echo json_encode($aprendizaje);
  }

  public function listFrasesByIdUsuario( $desde, $hasta ) {
    $desde = $this->date_model->frontDateToMySQLDate( $desde );
    $hasta = $this->date_model->frontDateToMySQLDate( $hasta );
    $idUsuario = $this->session->userdata['idUsuario'];

    $frases = $this->frase_model->listFrasesByIdUsuario($desde,$hasta,$idUsuario);

    echo json_encode($frases);
  }
}
?>