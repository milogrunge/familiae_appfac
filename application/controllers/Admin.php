<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
  function __contruct() {
    parent::__construct();
    $this->load->library('session');
  }

  function index(){
    $this->load->helper(array('form'));

    if( $this->session->userdata['idPerfil'] != ID_PERFIL_ADMINISTRADOR ) {
      header('Location:'.base_url().'usuario');
    }

    $this->load->view('admin_view');
  }
}
?>