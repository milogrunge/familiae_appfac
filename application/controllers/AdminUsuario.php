<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminUsuario extends CI_Controller {
  function __construct(){
    parent::__construct();
    $this->load->helper('form');
    $this->load->library('form_validation');
    $this->load->library('session');
    $this->load->model('usuario_model');
    $this->load->model('comuna_model');
    $this->load->model('date_model');
  }

  function index(){
    $this->load->helper(array('form'));

    $comunas = $this->comuna_model->listAllComuna();
    $usuarios = $this->usuario_model->listAllUsuarios();


    if( $this->session->userdata['idPerfil'] != ID_PERFIL_ADMINISTRADOR ) {
      header('Location:'.base_url().'usuario');
    }

    $this->load->view('adminusuario_view',['usuarios'=>$usuarios,'comunas'=>$comunas]);
  }

  public function eliminarUsuario($idUsuario) {
    echo json_encode( $this->usuario_model->eliminarUsuario( $idUsuario ) );
  }

  public function getUsuarioByIdUsuario($idUsuario) {
    $usuario = $this->usuario_model->getUsuarioByIdUsuario( $idUsuario )[0];
    $usuario->Nacimiento = $this->date_model->mySQLDateToFronDate( $usuario->Nacimiento );
    echo json_encode( $usuario );
  }
}
?>