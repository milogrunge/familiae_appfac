<!DOCTYPE html>
<html>
 <head>
   <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="/assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="/assets/img/favicon.png">  
  
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>Usuarios</title>

  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
  <meta name="viewport" content="width=device-width" />
  
  <link href="/assets/css/jquery-ui.css" rel="stylesheet" />  
  <link href="/assets/css/jquery-ui-1.10.0.custom.css" rel="stylesheet" />  
  <link href="/assets/css/bootstrap.css" rel="stylesheet" />
  <link href="/assets/css/gsdk.css" rel="stylesheet" />  
  <link href="/assets/css/jquery.notifications.css" rel="stylesheet" />  
  <link href="/assets/css/style.css" rel="stylesheet" /> 
    
  <!--     Font Awesome     -->
  <link href="/assets/css/font-awesome.css" rel="stylesheet">
  <link href='http://fonts.googleapis.com/css?family=Grand+Hotel' rel='stylesheet' type='text/css'>
 </head>
<body>

  <div class="modal fade" id="modalEliminarUsuario">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Eliminar Usuario</h4>
        </div>
        <div class="modal-body">
          <input type="hidden" name="idUsuario" id="idUsuario">
          ¿Está seguro que desea eliminar este usuario?
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
          <button type="button" class="btn btn-primary" id="doEliminarUsuario">Si</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modalUsuario">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Modal title</h4>
        </div>
        <div class="modal-body">
          <form id="formUsuario" role="form">
            <input type="hidden" name="idUsuario" id="idUsuario" >

            <div class="row">
              <div class="col-lg-6 col-md-6 col-xs-12">
                <div class="form-group" >
                  <label for="Usuario">Usuario</label>
                  <input type="text" name="Usuario" id="Usuario" class="form-control" required >
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-xs-12">
                <div class="form-group" >
                  <label for="Clave">Clave</label>
                  <input type="password" name="Clave" id="Clave" class="form-control">
                </div>
              </div>
            </div><!-- row -->

            <div class="row">
              <div class="col-lg-6 col-md-6 col-xs-12">
                <div class="form-group" >
                  <label for="Nombre">Nombre</label>
                  <input type="text" name="Nombre" id="Nombre" class="form-control" required >
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-xs-12">
                <div class="form-group" >
                  <label for="ApellidoP">Ap. paterno</label>
                  <input type="text" name="ApellidoP" id="ApellidoP" class="form-control" required >
                </div>
              </div>
            </div><!-- row -->
            
            <div class="row">
              <div class="col-lg-6 col-md-6 col-xs-12">
                <div class="form-group" >
                  <label for="ApellidoM">Ap. materno</label>
                  <input type="text" name="ApellidoM" id="ApellidoM" class="form-control" required >
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-xs-12">
                <div class="form-group" >
                  <label for="Nacimiento">Fecha nacimiento</label>
                  <input type="text" name="Nacimiento" id="Nacimiento" class="form-control dateDDMMYYYY" required >
                </div>
              </div>
            </div><!-- row -->
            
            <div class="row">
              <div class="col-lg-6 col-md-6 col-xs-12">
                <div class="form-group" >
                  <label for="Genero">Genero</label>
                  <select id="Genero" name="Genero" class="form-control">
                    <option value="">[Genero]</option>
                    <option value="F" >Femenino</option>
                    <option value="M" >Masculino</option>
                  </select>
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-xs-12">
                <div class="form-group" >
                  <label for="Comuna">Comuna</label>
                  <select id="Comuna_idComuna" name="Comuna_idComuna" class="form-control">
                    <option value="">[Comuna]</option>
                    <?php 
                      foreach ($comunas as $key => $comuna) {
                        if( $comuna->idComuna == $usuario->Comuna_idComuna ) {
                          echo '<option value="'.$comuna->idComuna.'" selected>'.$comuna->Nombre.'</option>';
                        } else {
                          echo '<option value="'.$comuna->idComuna.'">'.$comuna->Nombre.'</option>';
                        }
                      }
                    ?>
                  </select>
                </div>
              </div>
            </div><!-- row -->

            <div class="row">
              <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-xs-12">
                <button type="submit" id="doActualizarUsuario" class="btn btn-fill btn-primary form-control"><i class="fa fa-floppy-o"></i> Grabar</button>
              </div>
            </div><!-- row -->
            
            
          </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
        </div>
      </div>
    </div>
  </div>

  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Admin</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li class="active"><a href="<?php echo base_url(); ?>adminusuario">Usuarios </a></li>
          <li><a href="<?php echo base_url(); ?>resumenglobal">Resumen Global</a></li>
          <li><a href="<?php echo base_url(); ?>versugerencias">Sugerencias</a></li>
        </ul>
        
        <ul class="nav navbar-nav navbar-right">
          <li><a href="logout"><i class="fa fa-exit"></i>  Salir</a></li>
          
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>

  <div class="container">
    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
          <th width="5%">Id</th>
          <th width="">Nombres</th>
          <th width="">Ap. paterno</th>
          <th width="">Ap. materno</th>
          <th width="">Edad</th>
          <th width="10%">Acciones</th>
        </tr>
      </thead>
      <tbody>
        <?php 
          foreach ($usuarios as $key => $usuario) {
            echo '<tr>';
              echo "<td class=\"td-idUsuario\">$usuario->idUsuario</td>";
              echo "<td class=\"td-Nombre\">$usuario->Nombre</td>";
              echo "<td class=\"td-ApellidoP\">$usuario->ApellidoP</td>";
              echo "<td class=\"td-ApellidoM\">$usuario->ApellidoM</td>";
              echo "<td class=\"td-Edad\">$usuario->Edad</td>";
              echo '<td><button class="btn btn-primary btn-sm btnEditarUsuario" idUsuario="'.$usuario->idUsuario.'"><i class="fa fa-pencil"></i></button>
<button class="btn btn-danger btn-sm btnEliminarUsuario" idUsuario="'.$usuario->idUsuario.'"><i class="fa fa-trash-o"></i></button></td>';
            echo '</tr>';
          }
        ?>
      </tbody>
    </table>
  </div>


</body>

  <script src="/assets/js/jquery-1.10.2.js" type="text/javascript"></script>
  <!-- <script src="/assets/js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script> -->
  <script src="/assets/js/jquery-ui.js" type="text/javascript"></script>

  <script src="/assets/js/bootstrap.js" type="text/javascript"></script>
  <script src="/assets/js/gsdk-checkbox.js"></script>
  <script src="/assets/js/gsdk-radio.js"></script>
  <script src="/assets/js/gsdk-bootstrapswitch.js"></script>
  <script src="/assets/js/get-shit-done.js"></script>
  <script src="/assets/js/jquery.notifications.js"></script>
  <script src="/assets/js/custom.js"></script>
  <script src="/assets/js/views/adminusuario.js"></script>

<script type="text/javascript">
         
    $('.btn-tooltip').tooltip();
    $('.label-tooltip').tooltip();
    $('.pick-class-label').click(function(){
        var new_class = $(this).attr('new-class');  
        var old_class = $('#display-buttons').attr('data-class');
        var display_div = $('#display-buttons');
        if(display_div.length) {
        var display_buttons = display_div.find('.btn');
        display_buttons.removeClass(old_class);
        display_buttons.addClass(new_class);
        display_div.attr('data-class', new_class);
        }
    });
    $( "#slider-range" ).slider({
    range: true,
    min: 0,
    max: 500,
    values: [ 75, 300 ],
  });
  $( "#slider-default" ).slider({
      value: 70,
      orientation: "horizontal",
      range: "min",
      animate: true
  });
  $('.carousel').carousel({
    interval: 4000
  });
      
</script>