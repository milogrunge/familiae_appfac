<!DOCTYPE html>
<html>
 <head>
   <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="/assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="/assets/img/favicon.png">  
  
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>Reportes</title>

  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
  <meta name="viewport" content="width=device-width" />
  
  <link href="/assets/css/jquery-ui.css" rel="stylesheet" />  
  <link href="/assets/css/jquery-ui-1.10.0.custom.css" rel="stylesheet" />  
  <link href="/assets/css/bootstrap.css" rel="stylesheet" />
  <link href="/assets/css/gsdk.css" rel="stylesheet" />  
  <link href="/assets/css/style.css" rel="stylesheet" /> 
  <link href="/assets/css/jquery.notifications.css" rel="stylesheet" />  
  <link href="/assets/css/ui.daterangepicker.css" rel="stylesheet" />  
    
  <!--     Font Awesome     -->
  <link href="/assets/css/font-awesome.css" rel="stylesheet">
  <link href='http://fonts.googleapis.com/css?family=Grand+Hotel' rel='stylesheet' type='text/css'>
 </head>
<body>

  <div class="modal fade" id="modalGrafico">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Gráfico</h4>
        </div>
        <div class="modal-body">
          <div id="grafico"></div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modalFrases">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Frases</h4>
        </div>
        <div class="modal-body">
          <ul id="frases" class="list-group">
            
          </ul>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times"></i> Cerrar</button>
        </div>
      </div>
    </div>
  </div>

  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Nombre usuario</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li><a href="<?php echo base_url(); ?>editarperfil">Editar mi perfil</a></li>
          <li class="active"><a href="<?php echo base_url(); ?>reportesusuario">Reportes</a></li>
          <li><a href="<?php echo base_url(); ?>enviarsugerencia">Enviar sugerencia</a></li>
          
        </ul>
        
        <ul class="nav navbar-nav navbar-right">
          <li><a href="logout"><i class="fa fa-exit"></i>  Salir</a></li>
          
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>

  <div class="container">
  <form id="formFechas">
    <div class="row">
      <div class="col-lg-4 col-lg-offset-2 col-sm-6 col-xs-12 col-xs-offset-0">
        <div class="form-group">
          <label>Desde</label>
          <input type="text" name="fechaDesde" id="fechaDesde" class="form-control dateDDMMYYYY" required>
        </div>
      </div>
      <div class="col-lg-4 col-sm-6 col-xs-12">
        <div class="form-group">
          <label>Hasta</label>
          <input type="text" name="fechaHasta" id="fechaHasta" class="form-control dateDDMMYYYY" required>
        </div>
      </div>
    </div>
  </form>

    <div class="row">
      <div class="col-sm-4 col-xs-12">
        <div class="form-group text-center">
          <button type="button" class="btn btn-fill btn-success" id="btnFrases"><i class="fa fa-bars"></i> Frases</button>
        </div>
      </div>
      <div class="col-sm-4 col-xs-12">
        <div class="form-group text-center">
          <button type="button" class="btn btn-fill btn-primary" id="btnAprendizaje"><i class="fa fa-file-text-o"></i> Aprendizaje</button>
        </div>
      </div>
      <div class="col-sm-4 col-xs-12">
        <div class="form-group text-center">
          <button type="button" class="btn btn-fill btn-info" id="btnPalabras"><i class="fa fa-search"></i> Palabras</button>
        </div>
      </div>
    </div>
    

  </div><!-- container -->

</body>

<script src="/assets/js/jquery-1.10.2.js" type="text/javascript"></script>
  <script src="/assets/js/jquery-ui.js" type="text/javascript"></script>
  <script src="/assets/js/daterangepicker.jQuery.js" type="text/javascript"></script>

  <script src="/assets/js/bootstrap.js" type="text/javascript"></script>
  <script src="/assets/js/gsdk-checkbox.js"></script>
  <script src="/assets/js/gsdk-radio.js"></script>
  <script src="/assets/js/gsdk-bootstrapswitch.js"></script>
  <script src="/assets/js/get-shit-done.js"></script>
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/data.js"></script>
  <script src="https://code.highcharts.com/modules/drilldown.js"></script>
  <script src="/assets/js/jquery.notifications.js"></script>
  <script src="/assets/js/custom.js"></script>
  <script src="/assets/js/views/reportesusuario.js"></script>

<script type="text/javascript">
         
    $('.btn-tooltip').tooltip();
    $('.label-tooltip').tooltip();
    $('.pick-class-label').click(function(){
        var new_class = $(this).attr('new-class');  
        var old_class = $('#display-buttons').attr('data-class');
        var display_div = $('#display-buttons');
        if(display_div.length) {
        var display_buttons = display_div.find('.btn');
        display_buttons.removeClass(old_class);
        display_buttons.addClass(new_class);
        display_div.attr('data-class', new_class);
        }
    });
    $( "#slider-range" ).slider({
    range: true,
    min: 0,
    max: 500,
    values: [ 75, 300 ],
  });
  $( "#slider-default" ).slider({
      value: 70,
      orientation: "horizontal",
      range: "min",
      animate: true
  });
  $('.carousel').carousel({
    interval: 4000
  });
      
</script>