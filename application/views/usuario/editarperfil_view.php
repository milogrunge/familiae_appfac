<!DOCTYPE html>
<html>
 <head>
   <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="/assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="/assets/img/favicon.png">  
  
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>Editar perfil</title>

  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
  <meta name="viewport" content="width=device-width" />
  
  <link href="/assets/css/jquery-ui.css" rel="stylesheet" />  
  <link href="/assets/css/jquery-ui-1.10.0.custom.css" rel="stylesheet" />  
  <link href="/assets/css/bootstrap.css" rel="stylesheet" />
  <link href="/assets/css/gsdk.css" rel="stylesheet" />  
  <link href="/assets/css/jquery.notifications.css" rel="stylesheet" />  
  <link href="/assets/css/style.css" rel="stylesheet" /> 
    
  <!--     Font Awesome     -->
  <link href="/assets/css/font-awesome.css" rel="stylesheet">
  <link href='http://fonts.googleapis.com/css?family=Grand+Hotel' rel='stylesheet' type='text/css'>
 </head>
<body>
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">Nombre usuario</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
          <li class="active"><a href="<?php echo base_url(); ?>editarperfil">Editar mi perfil</a></li>
          <li><a href="<?php echo base_url(); ?>reportesusuario">Reportes</a></li>
          <li><a href="<?php echo base_url(); ?>enviarsugerencia">Enviar sugerencia</a></li>
          
        </ul>
        
        <ul class="nav navbar-nav navbar-right">
          <li><a href="logout"><i class="fa fa-exit"></i>  Salir</a></li>
          
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>

  <div class="container">
    <form id="formUsuario" role="form">
      <input type="hidden" name="idUsuario" id="idUsuario" value="<?php echo $usuario->idUsuario; ?>">

      <div class="row">
        <div class="col-lg-6 col-md-6 col-xs-12">
          <div class="form-group" >
            <label for="Usuario">Usuario</label>
            <input type="text" name="Usuario" id="Usuario" class="form-control" required value="<?php echo $usuario->Usuario ?>">
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-xs-12">
          <div class="form-group" >
            <label for="Clave">Clave</label>
            <input type="password" name="Clave" id="Clave" class="form-control">
          </div>
        </div>
      </div><!-- row -->

      <div class="row">
        <div class="col-lg-6 col-md-6 col-xs-12">
          <div class="form-group" >
            <label for="Nombre">Nombre</label>
            <input type="text" name="Nombre" id="Nombre" class="form-control" required value="<?php echo $usuario->Nombre ?>">
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-xs-12">
          <div class="form-group" >
            <label for="ApellidoP">Ap. paterno</label>
            <input type="text" name="ApellidoP" id="ApellidoP" class="form-control" required value="<?php echo $usuario->ApellidoP?>">
          </div>
        </div>
      </div><!-- row -->
      
      <div class="row">
        <div class="col-lg-6 col-md-6 col-xs-12">
          <div class="form-group" >
            <label for="ApellidoM">Ap. materno</label>
            <input type="text" name="ApellidoM" id="ApellidoM" class="form-control" required value="<?php echo $usuario->ApellidoM ?>">
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-xs-12">
          <div class="form-group" >
            <label for="Nacimiento">Fecha nacimiento</label>
            <input type="text" name="Nacimiento" id="Nacimiento" class="form-control dateDDMMYYYY" required value="<?php echo $usuario->Nacimiento ?>">
          </div>
        </div>
      </div><!-- row -->
      
      <div class="row">
        <div class="col-lg-6 col-md-6 col-xs-12">
          <div class="form-group" >
            <label for="Genero">Genero</label>
            <select id="Genero" name="Genero" class="form-control">
              <option value="">[Genero]</option>
              <option value="F" <?php echo $usuario->Genero=='F'?'selected':''; ?> >Femenino</option>
              <option value="M" <?php echo $usuario->Genero=='M'?'selected':''; ?>>Masculino</option>
            </select>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-xs-12">
          <div class="form-group" >
            <label for="Comuna">Comuna</label>
            <select id="Comuna_idComuna" name="Comuna_idComuna" class="form-control">
              <option value="">[Comuna]</option>
              <?php 
                foreach ($comunas as $key => $comuna) {
                  if( $comuna->idComuna == $usuario->Comuna_idComuna ) {
                    echo '<option value="'.$comuna->idComuna.'" selected>'.$comuna->Nombre.'</option>';
                  } else {
                    echo '<option value="'.$comuna->idComuna.'">'.$comuna->Nombre.'</option>';
                  }
                }
              ?>
            </select>
          </div>
        </div>
      </div><!-- row -->

      <div class="row">
        <div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-xs-12">
          <button type="submit" id="btnActualizarUsuario" class="btn btn-fill btn-primary form-control"><i class="fa fa-floppy-o"></i> Grabar</button>
        </div>
      </div><!-- row -->
      
      
    </form>
  </div>

</body>

<script src="/assets/js/jquery-1.10.2.js" type="text/javascript"></script>
  <!-- <script src="/assets/js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script> -->
  <!-- <script src="/assets/js/jquery-ui.custom.min.js" type="text/javascript"></script> -->
  <script src="/assets/js/jquery-ui.js" type="text/javascript"></script>

  <script src="/assets/js/bootstrap.js" type="text/javascript"></script>
  <script src="/assets/js/gsdk-checkbox.js"></script>
  <script src="/assets/js/gsdk-radio.js"></script>
  <script src="/assets/js/gsdk-bootstrapswitch.js"></script>
  <script src="/assets/js/get-shit-done.js"></script>
  <script src="/assets/js/jquery.notifications.js"></script>
  <script src="/assets/js/custom.js"></script>
  <script src="/assets/js/views/editarperfil.js"></script>

<script type="text/javascript">
         
    $('.btn-tooltip').tooltip();
    $('.label-tooltip').tooltip();
    $('.pick-class-label').click(function(){
        var new_class = $(this).attr('new-class');  
        var old_class = $('#display-buttons').attr('data-class');
        var display_div = $('#display-buttons');
        if(display_div.length) {
        var display_buttons = display_div.find('.btn');
        display_buttons.removeClass(old_class);
        display_buttons.addClass(new_class);
        display_div.attr('data-class', new_class);
        }
    });
    $( "#slider-range" ).slider({
    range: true,
    min: 0,
    max: 500,
    values: [ 75, 300 ],
  });
  $( "#slider-default" ).slider({
      value: 70,
      orientation: "horizontal",
      range: "min",
      animate: true
  });
  $('.carousel').carousel({
    interval: 4000
  });
      
</script>