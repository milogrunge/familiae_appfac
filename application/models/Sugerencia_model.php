<?php
Class Sugerencia_model extends CI_Model{

  public function enviarSugerencia($sugerencia) {
    
    $this->db->insert('Sugerencia', $sugerencia);
    return 1;
  }

  public function listAllSugerencias() {
    $this->db->select('s.Palabra, s.Imagen, s.Porque, CONCAT_WS(\' \',u.Nombre, u.ApellidoP, u.ApellidoM) AS Usuario');
    $this->db->from('Sugerencia s');
    $this->db->join('Usuario u','u.IdUsuario = s.Usuario_idUsuario');

    $query = $this->db->get();
    if($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }
}
?>