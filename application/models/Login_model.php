<?php
Class Login_model extends CI_Model{
public function login($Usuario, $Clave){
   $this -> db -> select('idUsuario, Usuario, Clave');
   $this -> db -> from('Usuario');
   $this -> db -> where('Usuario', $Usuario);
   $this -> db -> where('Clave', $Clave);
   $this -> db -> limit(1);
   $query = $this -> db -> get();
   if($query -> num_rows() == 1){
     return $query->result();
   }else{
     return false;
   }
 }
//Leer los datos para devolverlas en variable de sesion
public function read_user_information($Usuario) {
    $this->db->select('*');
    $this->db->from('Usuario');
    $this->db->where('Usuario', $Usuario);
    $this->db->limit(1);
    $query = $this->db->get();

    if ($query->num_rows() == 1) {
     return $query->result();
    } else {
       return false;
    }
  }
}
?>