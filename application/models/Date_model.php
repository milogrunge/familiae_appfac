<?php
Class Date_model extends CI_Model{
  public function frontDateToMySQLDate($strDate){
    if(empty($strDate) || $strDate=='' || $strDate==null || $strDate=='1970-01-01' || $strDate=='0000-00-00'){
      return null;
    }
    return date('Y-m-d', strtotime(str_replace("/","-",$strDate)));
  }

  public function mySQLDateToFronDate($strDate){
    if($strDate=='null' || $strDate==null || $strDate==''){
      return '';
    }
    $fronDate =date('d/m/Y',strtotime($strDate));
    if($fronDate=='01/01/1970' || $fronDate=='30/11/-0001'){
      return '';
    }
    return $fronDate;
  }
}
?>