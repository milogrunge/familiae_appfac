<?php
Class Aprendizaje_model extends CI_Model{
  

  public function contarAprendizajeCorrecto($desde, $hasta, $idUsuario) {
    $this->db->select('COUNT(1) AS total');
    $this->db->from('HistorialAprendizaje ha');
    $this->db->where('ha.Usuario_idUsuario',$idUsuario);
    $this->db->where("ha.Palabra_idPalabra = Palabra_idRespuesta");
    $this->db->where("ha.Fecha BETWEEN '$desde' AND '$hasta'");

    $query = $this->db->get();


    if($query->num_rows() > 0 ) {
      return $query->result();
    } else {
      return false;
    }
  }

  public function contarAprendizajeIncorrecto($desde, $hasta, $idUsuario) {
    $this->db->select('COUNT(1) AS total');
    $this->db->from('HistorialAprendizaje ha');
    $this->db->where('ha.Usuario_idUsuario',$idUsuario);
    $this->db->where("ha.Palabra_idPalabra <> Palabra_idRespuesta");
    $this->db->where("ha.Fecha BETWEEN '$desde' AND '$hasta'");

    $query = $this->db->get();


    if($query->num_rows() > 0 ) {
      return $query->result();
    } else {
      return false;
    }
  }
}
?>