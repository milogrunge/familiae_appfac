<?php
Class Frase_model extends CI_Model{

  public function listFrasesByIdUsuario($desde, $hasta, $idUsuario) {
    $this->db->select('f.idFrase, p.Nombre, fhp.Orden');
    $this->db->from('Frase f');
    $this->db->join('Frase_has_Palabra fhp','fhp.Frase_idFrase = f.idFrase');
    $this->db->join('Palabra p','p.idPalabra = fhp.Palabra_idPalabra');
    $this->db->where('f.Usuario_idUsuario',$idUsuario);
    $this->db->order_by('idFrase','ASC');
    $this->db->order_by('Orden','ASC');

    $query = $this->db->get();

    if($query->num_rows() > 0 ) {
      return $query->result();
    } else {
      return false;
    }
  }

}
?>