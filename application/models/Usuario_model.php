<?php
Class Usuario_model extends CI_Model{

  public function getUsuarioByIdUsuario($idUsuario) {
    $this->db->select('*');
    $this->db->from('Usuario');
    $this->db->where('idusuario',$idUsuario);
    $this->db->limit(1);
    
    $query = $this->db->get();
    if($query->num_rows() == 1 ) {
      return $query->result();
    } else {
      return false;
    }
  }

  public function actualizarPerfil($usuario) {
    $this->db->where('idUsuario',$usuario['idUsuario']);
    unset($usuario['idUsuario']);
    if( empty($usuario['Clave']) ) {
      unset($usuario['Clave']);
    }
    $this->db->update('Usuario',$usuario);
    return 1;
  }

  public function listAllUsuarios() {
    $this->db->select('idUsuario, Nombre, ApellidoP, ApellidoM, floor(datediff(curdate(),Nacimiento) / 365) AS Edad');
    $this->db->from('Usuario');
    $this->db->where('Perfil_idPerfil=2');
    
    $query = $this->db->get();
    if($query->num_rows() > 0) {
      return $query->result();
    } else {
      return false;
    }
  }

  public function eliminarUsuario($idUsuario) {
    $this->db->where('idUsuario',$idUsuario);
    $this->db->delete('Usuario');

    return 1;
  }
}
?>