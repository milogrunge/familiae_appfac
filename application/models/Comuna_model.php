<?php
Class Comuna_model extends CI_Model{
  //Obtener repetición de palabras
  public function listAllComuna() {
    $this->db->select('*');
    $this->db->from('Comuna');
    $this->db->order_by('Nombre','asc');
    
    $query = $this->db->get();
    if($query->num_rows() > 0 ) {
      return $query->result();
    } else {
      return false;
    }
  }
}
?>