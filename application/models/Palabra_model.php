<?php
Class Palabra_model extends CI_Model{
  //Obtener repetición de palabras
  public function listRepeticionPalabraPorRangoMeses($mes1, $mes2) {
    $this->db->select('COUNT(p.idPalabra) as total,p.idPalabra ,p.nombre');
    $this->db->from('Palabra p');
    $this->db->join('Frase_has_Palabra fhp', 'fhp.Palabra_idPalabra = p.idPalabra');
    $this->db->join('Frase f', 'f.idFrase = fhp.Frase_idFrase');
    $this->db->where('YEAR(f.fecha) = YEAR(now())');
    $this->db->where("MONTH(f.fecha) between $mes1 and $mes2");
    $this->db->group_by('p.idPalabra');
    $this->db->order_by('total', 'desc');

    $query = $this->db->get();
    if($query->num_rows() > 0 ) {
      return $query->result();
    } else {
      return false;
    }
  }

  public function listRepeticionPalabrasByIdUsuarioAndRango($desde, $hasta, $idUsuario) {
    $this->db->select('COUNT(p.idPalabra) as total,p.idPalabra ,p.nombre');
    $this->db->from('Palabra p');
    $this->db->join('Frase_has_Palabra fhp', 'fhp.Palabra_idPalabra = p.idPalabra');
    $this->db->join('Frase f', 'f.idFrase = fhp.Frase_idFrase');
    $this->db->where('f.Usuario_idUsuario',$idUsuario);
    $this->db->where("f.fecha BETWEEN '$desde' AND '$hasta'");
    $this->db->group_by('p.idPalabra');
    $this->db->order_by('total', 'desc');

    $query = $this->db->get();


    if($query->num_rows() > 0 ) {
      return $query->result();
    } else {
      return false;
    }
  }
}
?>